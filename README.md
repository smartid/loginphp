# README #

This sample application is one of the easiest ways to get up and running with Smart-ID secure login service in PHP.

### How it will look like? ###
http://ppo.ee/smartid/loginphp/

### How do I get set up? ###
```
* Go to https://id.smartid.ee and register your website https://mydomain.ee. Set Oauth redirect_uri  to be https://mydomain.ee/loginphp.
* Copy the "OAuth clientId/secret" and redirect_uri values to the sample app index.php
* Open mydomain.ee/loginphp/index.php and click on eIDAS logo to start the login process!
```



### Who do I talk to? ###

Please turn to help@smartid.ee with all of the questions